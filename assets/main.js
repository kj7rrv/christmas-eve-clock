/*
 * Copyright (c) 2022 Samuel Sloniker
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

function daysUntilChristmas(now) {
    let ms_per_day = 24*60*60*1000
    let christmas

    if (now.getMonth() == 11 && now.getDate() == 25) {
        christmas = now
    } else if (now.getMonth() == 11 && now.getDate() > 25) {
        christmas = new Date(now.getFullYear() + 1, 11, 25)
    } else {
        christmas = new Date(now.getFullYear(), 11, 25)
    }

    ms_until_christmas = christmas - now
    return Math.ceil(ms_until_christmas / ms_per_day)
}

function adjust() {
    let ce = document.querySelector("#christmaseve") 
    let cec = document.querySelector("#christmasevebox") 
    let cet = document.querySelector("#christmasevetext") 

    let fontSize = 1
    ce.style.fontSize = "1px"

    while (ce.clientHeight <= cec.clientHeight && cec.scrollWidth <= document.documentElement.clientWidth && fontSize <= 80) {
        fontSize += 1
        ce.style.fontSize = String(fontSize) + "px"
    }

    ce.style.fontSize = String(fontSize - 1) + "px"
}

function share(e) {
    navigator.share({title: document.querySelector("#christmaseve").textContent, url: "https://christmaseveclock.com"})
}

function setup(now) {
    let christmaseve = document.querySelector("#christmasevetext")
    let sharebox = document.querySelector("#sharebox")

    christmaseve.textContent = "Merry Christmas" + " Eve".repeat(daysUntilChristmas(now)) + "!"

    if (navigator.canShare && navigator.canShare({title: "test", url: "https://christmaseveclock.com"})) {
        sharebox.style.display = "block"
        sharebox.addEventListener("click", share)
    }

    adjust()
}

function main() {
    let hasValidDate = false
    let now
    let urlParams = new URLSearchParams(location.search)
    let dateParam = urlParams.get('now')
    let parts
    if (dateParam !== null) {
        parts = dateParam.split("-")
        if (parts.length == 3) {
            let year = Number(parts[0])
            let month = Number(parts[1])-1
            let day = Number(parts[2])
            now = new Date(year, month, day)
            hasValidDate = (! isNaN(now.getFullYear())) && now.getFullYear() === year && now.getMonth() === month && now.getDate() == day
        }
    }
    if (! hasValidDate) {
        now = new Date()
    }
    setup(now)
    window.addEventListener("resize", adjust)
}

window.onload = main
